# `@peepso/prettier-config`

Prettier shareable configuration for projects at PeepSo.

## Installation

```bash
npm install --save-dev prettier @peepso/prettier-config
```

## Usage

**Edit `package.json`**:

```jsonc
{
  // ...
  "prettier": "@peepso/prettier-config"
}
```
